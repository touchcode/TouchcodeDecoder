// swift-tools-version:5.3

import PackageDescription

let package = Package(
    name: "TouchcodeDecoder",
    platforms: [
        .iOS(.v10)
    ],
    products: [
        // Products define the executables and libraries a package produces, and make them visible to other packages.
        .library(
            name: "TouchcodeDecoder",
            targets: ["TouchcodeDecoder"])
    ],
    dependencies: [
      .package(
          name: "Alamofire",
          url: "https://github.com/Alamofire/Alamofire.git", .exact("4.9.1")
      ),
      .package(
          name: "CryptoSwift",
          url: "https://github.com/krzyzanowskim/CryptoSwift.git", .exact("1.3.3")
      ),
      .package(
          name: "NetUtils",
          url: "https://github.com/svdo/swift-netutils.git", .exact("4.1.0")
      )
],
    targets: [
        .binaryTarget(
            name: "TouchcodeDecoder",
            path: "./TouchcodeDecoder.xcframework"
        )
    ]
)
